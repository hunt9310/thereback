<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage thereback
 */

get_header(); ?>
<div id="second-content" class="second-content">

	<div id="secondary" class="content-area">
		<div id="content" class="site-content" role="main">
			<div class="news"> 
			  <div class="row">
			    <div class="text-section">
			      <div class="fullText main">
			        <div class="bg">
			          <img src="http://www.armorsecurityandprotection.com/thereandback/wp-content/uploads/2016/10/11846645_821359684645862_4957723937905001786_n.jpg" />  
			        </div>
			        <div class="title">News</div>
			        <p>A former professional lacrosse player and college football and lacrosse standout who struggled with alcohol and substance abuse. He is now a motivational speaker who talks to high school athletes about the dangers of substance abuse.</p>
			      </div>
			      <div class="fullText">
			        <div class="text">
						<div class="right">
							<?php if ( have_posts() ) : ?>
								<header class="page-header">
									<h1 class="page-title">
										<?php
											if ( is_day() ) :
												printf( __( 'Daily Archives: %s', 'twentyfourteen' ), get_the_date() );

											elseif ( is_month() ) :
												printf( __( 'Monthly Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyfourteen' ) ) );

											elseif ( is_year() ) :
												printf( __( 'Yearly Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentyfourteen' ) ) );

											else :
												_e( 'Archives', 'twentyfourteen' );

											endif;
										?>
									</h1>
								</header><!-- .page-header -->

								<?php
										// Start the Loop.
										while ( have_posts() ) : the_post();

											/*
											 * Include the post format-specific template for the content. If you want to
											 * use this in a child theme, then include a file called called content-___.php
											 * (where ___ is the post format) and that will be used instead.
											 */
											get_template_part( 'content', get_post_format() );

										endwhile;
										// Previous/next page navigation.
										twentyfourteen_paging_nav();

									else :
										// If no content, include the "No posts found" template.
										get_template_part( 'content', 'none' );

									endif;
								?>
						</div>
						<div class="left">
				          <ul>
				            <li>
				              <a href="donate/" class="side_button">Donate Today!</a>
				            </li>
				            <li>
				              <div class="title">Latest News</div><?php get_sidebar( 'content' ); ?><?php get_sidebar(); ?> 
				            </li>
				            <li>
				              <a href='https://www.facebook.com/ENDTHEDENIAL' class='symbol' title='facebook'>Like Us</a>
				            </li>
				            <li>
				              <a href='https://twitter.com/kenbartolo10' class='symbol' title='twitterbird'>Follow us</a>
				            </li>
				            <li>
				              <a href="contact/" class="symbol" title="imessage">Conatct Us</a>
				            </li>
				          </ul>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();
