<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage thereback
 */

?>

		</div><!-- #main -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="col-md-3">
				<div class="inner">
					<div class="title">Stay Connected</div>
					<ul class="social">
						<li class="fb"><a href="https://www.facebook.com/ENDTHEDENIAL" target="_blank"><svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	 x="0px" y="0px" viewBox="0 0 125 125" xml:space="preserve">
<path fill="#FFFFFF" d="M83,47.4c-0.5,4.6-0.9,8.8-1.5,13c-0.1,0.6-1.3,1.5-2,1.5c-2.6,0.2-5.2,0.2-7.8,0c-2.1-0.1-2.7,0.6-2.7,2.7
	c0.1,13.1,0,26.1,0.1,39.2c0,2.6-0.6,3.6-3.4,3.5c-3.9-0.2-7.8-0.2-11.7,0c-2.3,0.1-3-0.7-3-3c0.1-13,0-25.9,0-38.9c0-1,0-2,0-3.4
	c-2.5,0-4.7-0.1-6.8,0c-1.7,0.1-2.3-0.6-2.3-2.3c0.1-3.4,0-6.9,0-10.3c0-1.5,0.6-2.3,2.2-2.1c0.1,0,0.2,0,0.3,0
	c6.7-0.2,6.6-0.2,6.8-6.9c0.1-3.3,0.1-6.7,0.7-9.9c1.5-7.4,6.7-11.9,14.6-12.6c4.8-0.4,9.6-0.2,14.4-0.3c1.6,0,2.1,0.7,2.1,2.2
	c-0.1,3.3-0.1,6.7,0,10c0.1,1.8-0.8,2.2-2.3,2.2c-2.6,0-5.2,0.2-7.7,0.5c-2.3,0.3-3.7,1.7-3.8,4.1c-0.1,3,0,5.9-0.1,8.9
	c0,1.8,1,1.9,2.4,1.9C75.2,47.4,78.9,47.4,83,47.4z"/>
</svg>
</a></li>
						<li class="tw"><a href="https://twitter.com/kenbartolo10" target="_blank"><svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	 x="0px" y="0px" viewBox="0 0 125 125" xml:space="preserve">
<path fill="#FFFFFF" d="M76,23c3,0,6,0,8.9,0c3.2,1.9,6.3,3.8,9.6,5.6c0.7,0.4,1.7,0.3,2.5,0.1c3.5-1.1,6.9-2.4,10.4-3.6
	c0.2,0.2,0.4,0.3,0.6,0.5c-2.4,3.2-4.7,6.3-7.3,9.7c3.4-0.9,6.5-1.8,9.7-2.6c0,0.2,0,0.5,0,0.7c-2.3,2.4-4.4,5-6.9,7
	c-2.1,1.7-2.7,3.4-2.9,6c-1.7,24-12.6,41.9-35,51.7c-4.3,1.9-9.1,2.7-13.6,4c-5,0-10.1,0-15.1,0c-0.7-0.2-1.5-0.5-2.2-0.7
	c-7.2-1.3-13.9-3.6-20-8c10.1,0.4,19.2-2,27.5-8.2C32,82.9,26,78,24.7,70.9c2.2,0,4.4,0,7.2,0c-10.9-6.3-15.6-13-14-19.6
	c2.4,0.6,4.7,1.3,7.9,2.1c-8.9-8.9-10.8-19.5-5.3-26.5C31.2,39.1,44.3,46.3,61,47.2C59.9,41,61.1,35.8,64.4,31
	C67.3,27,71.6,24.9,76,23z"/>
</svg>
</a></li>
<!-- 						<li class="in"><a href="#"><svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	 x="0px" y="0px" viewBox="0 0 125 125" xml:space="preserve">
<g id="cmUMfG.tif">
	<g>
		<path fill="#FFFFFF" d="M22,30.6c3.6-6,7.6-7.9,13.3-6.2c4.3,1.3,6.8,5.1,6.4,9.8c-0.4,4-3.7,7.3-7.8,7.8c-5.8,0.7-8.8-1-11.9-6.7
			C22,33.7,22,32.2,22,30.6z"/>
		<path fill="#FFFFFF" d="M50.1,101.1c0-17.4,0-34.6,0-52.2c3,0,6,0,9,0c0.2,0,0.3,0,0.5,0c2.3,0,5.1-0.7,6.8,0.3
			c1.2,0.7,1.1,3.8,1.6,6c3.4-4.7,8-7.2,13.8-7.5c10.9-0.5,18.8,5.8,20.6,16.6c0.4,2.1,0.5,4.3,0.5,6.4c0.1,10,0,20,0,30.3
			c-5.8,0-11.4,0-17.5,0c0-1,0-2,0-3c0-9.1,0.1-18.1-0.2-27.2c-0.2-7.5-6.5-11.4-12.6-8.2c-3.1,1.6-5,4.3-5,7.9
			c-0.1,9.1-0.1,18.1-0.1,27.2c0,1,0,2,0,3.3C61.7,101.1,56,101.1,50.1,101.1z"/>
		<path fill="#FFFFFF" d="M40.5,101.2c-5.8,0-11.4,0-17.2,0c0-17.4,0-34.6,0-52.1c5.7,0,11.4,0,17.2,0
			C40.5,66.4,40.5,83.6,40.5,101.2z"/>
	</g>
</g>
</svg></a></li> -->
					</ul>
				</div>
			</div>
			<div class="col-md-3">
				<div class="inner">
					<div class="title">Get Involved</div>
					<p>All Donations directly support the mission of There and Back, Inc. We appreciate any and all Donations.</p>
					<a href="donate/" class="footer_button">Donate</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="inner">
					<div class="title">Our News</div>
					<p>Keynote Speaker Ken Bartolo will be doing an exclusive interview with ABC Channel 9 on Monday September 26th.</p>
					<a href="news/" class="footer_button">See More</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="inner">
					<div class="title">Contact Us</div>
					<div class="name">There and Back, Inc.</div>
					<div class="address"><div class="top">Po Box 261</div><div class="bottom">Fayetteville, NY 13066</div></div>
					<div class="phone">(315) 807-3905 and <br> (315) 766-6407</div>
				</div>
			</div>

			<?php get_sidebar( 'footer' ); ?>

		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>